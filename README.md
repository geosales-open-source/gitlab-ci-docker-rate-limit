# Gitlab Ci Docker Rate Limit

Estudando como contornar o problema do Docker Rate Limit através do GitLab-CI

## Possíveis soluções

### Configuração do Runner
- Adição do *snippet* no arquivo de configuração do *Gitlab Runner*
- Arquivo localizado em, ambiente Linux: /etc/gitlab-runner/config.toml

> Necessário estar rodando a versão 13.8 do gitlab-runner ou posterior; testes com a 13.2 causam falha ao tentar alterar a configuração

```makefile
[runners.docker]
  pull_policy = ["if-not-present"]
```

- pull_policy possui três valores, podendo combináá-los para serem executados em sequência até um "pull" acontecer ou as possibilidades serem exauridas
    - never 
        - Não faz o "pull" da imagem
    - if-not-present
        - Faz o "pull" apenas se a imagem não existir
    - always
        - Sempre faz o "pull"

[Teste realizado sem limitação real do Docker Hub](https://gitlab.com/henrybarreto/test-ci/-/jobs/1371779147#L4)
